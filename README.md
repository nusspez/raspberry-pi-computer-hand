# raspberry pi computer hand

(on working) computadora de mano creada con raspberry pi con el fin de usarla como calculadora

## Getting Started

el fin de construirla es usarla como calculadora que permita resolver derivadas,integrales,metodos numericos,etc.

## Prerequisites

### hardware
- raspberry pi3
- sd card de minimo 8gb
- pantalla de 5 pulgadas
- teclado bluetooth
- powerbank minimo 2 amperes
- raspberry pi case(https://www.thingiverse.com/thing:3695709)

## instalacion

1. Actualizando el sistema 

```
sudo apt-get update && apt-get upgrade
```

2. bajar el controlador de la pantalla touch(opcianal)

https://github.com/saper-2/rpi-5inch-hdmi-touchscreen-driver(segir las instrucciones en el siguiente repositorio)


